//geojson by seasons4
/*
var zone_butinage_spring = new ol.layer.Vector({
    source: new ol.source.Vector({
        url: '../JS/DATA/pt_butinage_spring.geojson',
        format: new ol.format.GeoJSON({ featureProjection: "EPSG:3857" })
    }),
    visible: false,
    title: 'ZONE DE BUTINAGE SPRING',
});

var zone_butinage_summer = new ol.layer.Vector({
    source: new ol.source.Vector({
        url: '../JS/Data/pt_butinage_summer.geojson',
        format: new ol.format.GeoJSON({ featureProjection: "EPSG:3857" })
    }),
    visible: false,
    title: 'ZONE DE BUTINAGE SUMMER',
});

var zone_butinage_autumn = new ol.layer.Vector({
    source: new ol.source.Vector({
        url: '../JS/Data/pt_butinage_autumn.geojson',
        format: new ol.format.GeoJSON({ featureProjection: "EPSG:3857" })
    }),
    visible: false,
    title: 'ZONE DE BUTINAGE AUTUMN ',
});

*/



//-----------------------------heatmap -------------------

//heatmap : Spring


var blur = document.getElementById('blur');
var radius = document.getElementById('radius');








//----- fonction d'affichage de heatmap----------------
function heatmap_calculator(dir) {



    var source = new ol.source.Vector({
        url: dir,
        format: new ol.format.GeoJSON({ dataProjection: "EPSG:3857" })
    });

    var heatmapLayer = new ol.layer.Heatmap({
        title: "heatmap",
        name: "heatmap",
        displayInLayerSwitcher: false,
        source: source,
        blur: parseInt(blur.value, 10),
        radius: parseInt(radius.value, 10),
        gradient: ["#00f", "#0ff", "#0f0", "#ff0", "#f00"],
        //bleu cyan vert jaune rouge
        weight: function (feature) {
            return 10;
        }
    })

    return heatmapLayer
}








//----------------------------------Lecture et interogation d'un  fichier Geojson
// --- filtre


//-------------- selected season-------------
$('#selected_season').on('click', function () {
    map.getLayers().forEach(function (layer) {
        //console.log(layer.get('title'))
        if (layer !== undefined) {

            if (layer.get("name") !== undefined) {
                if (

                    layer.get("name") === "heatmap"
                )
                    map.removeLayer(layer);
            }
        }





    });
    //map.removeLayer(heatmapLayer)


    const choice = $(this).val();
    switch (choice) {

        case 'Summer':
            dir = '../JS/Data/pt_butinage_summer.geojson'
            heatmapLayer = heatmap_calculator(dir)
            var extent = heatmapLayer.getSource().getExtent();
            console.log(extent)
            map.addLayer(heatmapLayer);
            map.getView().fit(extent, map.getSize());

            break;
        case 'Spring':
            dir = '../JS/DATA/pt_butinage_spring.geojson'
            heatmapLayer = heatmap_calculator(dir)
            map.addLayer(heatmapLayer);
            break;
        case 'Autumn':
            dir = '../JS/Data/pt_butinage_autumn.geojson'
            heatmapLayer = heatmap_calculator(dir)
            map.addLayer(heatmapLayer);
            break;
    }
    blur.addEventListener('input', function () {
        heatmapLayer.setBlur(parseInt(blur.value, 10));
    });

    radius.addEventListener('input', function () {
        heatmapLayer.setRadius(parseInt(radius.value, 10));
    });
   
    //map.getView().fitExtent(extent, map.getSize());
});


//listner 

