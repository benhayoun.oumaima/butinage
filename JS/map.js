
// key of wms tiles
var key = "29nhgfdgnp27yog52o8i4j4u";

// OpenStreetMap
var OSM = new ol.layer.Tile({
  source: new ol.source.OSM(),
  visible: true,
  title: 'OPENSTREETMAP',
});

// Tile Grid parameters
var resolutions = [];
var matrixIds = [];
var proj3857 = ol.proj.get("EPSG:3857");
var maxResolution = ol.extent.getWidth(proj3857.getExtent()) / 256;

for (var i = 0; i < 18; i++) {
  resolutions[i] = maxResolution / Math.pow(2, i);
}

tileGrid = new ol.tilegrid.WMTS({
  origin: [-20037508, 20037508],
  resolutions: resolutions,
  matrixIds: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"],
})
/*
//WMS Tile of BDORTHO
var BDORTHO=new ol.layer.Tile({
  opacity: 0.7,
  source: new ol.source.WMTS({
  url: "https://wxs.ign.fr/decouverte/geoportail/wmts?",
  layer: "ORTHOIMAGERY.ORTHOPHOTOS",
  matrixSet: "PM",
  format: "image/jpeg",
  projection: "EPSG:3857",
  tileGrid:tileGrid,
  style: 'normal',
  }),
  title:'BDORTHO',
  visible: false
});
*/
//WMS Tile of BDSCAN
var BDSCAN = new ol.layer.Tile({
  opacity: 0.7,
  source: new ol.source.WMTS({
    url: "https://wxs.ign.fr/cartes/geoportail/wmts?",
    layer: "GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2",
    matrixSet: "PM",
    format: "image/png",
    projection: "EPSG:3857",
    tileGrid: tileGrid,
    style: 'normal',
  }),
  title: 'BDSCAN',
  visible: false
});

//WMTS Satellite
var PLEADE = new ol.layer.Tile({
  opacity: 0.7,
  source: new ol.source.WMTS({
    url: "https://wxs.ign.fr/satellite/geoportail/wmts?",
    layer: "ORTHOIMAGERY.ORTHO-SAT.PLEIADES.2021",
    matrixSet: "PM",
    format: "image/png",
    projection: "EPSG:3857",
    tileGrid: tileGrid,
    style: 'normal',
  }),
  title: 'PLEADE',
  visible: false
});

//test
var projection = ol.proj.get('EPSG:3857');
var tileSizePixels = 256;
var tileSizeMtrs = ol.extent.getWidth(projection.getExtent()) / tileSizePixels;
var matrixIds = [];
var resolutions = [];
for (var i = 0; i <= 25; i++) {
  matrixIds[i] = i;
  resolutions[i] = tileSizeMtrs / Math.pow(2, i);
}
var tileGrid = new ol.tilegrid.WMTS({
  origin: ol.extent.getTopLeft(projection.getExtent()),
  resolutions: resolutions,
  extent: [-81114.89501367561752, 5809433.873248805291951, -72287.919089834147599, 5800566.797621245495975],
  matrixIds: matrixIds
});

var BDORTHO = new ol.layer.Tile({
  source: new ol.source.WMTS({
    attributions: ["IGN-F/Géoportail"],
    url: 'https://wxs.ign.fr/decouverte/geoportail/wmts',
    layer: 'ORTHOIMAGERY.ORTHOPHOTOS',
    matrixSet: 'PM',
    format: 'image/jpeg',
    tileGrid: tileGrid,
    style: 'normal',
    extent: [-81114.89501367561752, 5809433.873248805291951, -72287.919089834147599, 5800566.797621245495975],
    dimensions: {
      'threshold': 100,
    },
  }),
  title: 'BDORTHO',
  visible: false
})

/*
//test
var extent = [420000, 30000, 900000, 350000];
var test= new ol.layer.Tile({
  extent: extent,
  source: new ol.source.TileWMS({
    url: 'https://wms.geo.admin.ch/',
    crossOrigin: 'anonymous',
    attributions: '© <a href="http://www.geo.admin.ch/internet/geoportal/' +
        'en/home.html">Pixelmap 1:1000000 / geo.admin.ch</a>',
    params: {
      'LAYERS': 'ch.swisstopo.pixelkarte-farbe-pk1000.noscale',
      'FORMAT': 'image/jpeg'
    },
    serverType: 'mapserver'
  })
}),
*/
/*
//study_area 
var zone_etude_3=new  ol.layer.Vector({
  source: new ol.source.Vector({
    url: '../JS/Data/Zone_etude_3km.geojson',
    format: new ol.format.GeoJSON()
  }),
  visible: false,
  title: 'ZONE ETUDE (3km)',
});
*/

//study area (5km)
var zone_etude = new ol.layer.Vector({
  source: new ol.source.Vector({
    url: '../JS/Data/pt_5m.geojson',
    format: new ol.format.GeoJSON()
  }),
  visible: false,
  title: 'ZONE ETUDE',
});

//points de butinage (format geojson)
var zone_butinage = new ol.layer.Vector({
  source: new ol.source.Vector({
    url: '../JS/Data/Pt_butinage_2km.geojson',
    format: new ol.format.GeoJSON({ featureProjection: "EPSG:3857" })
  }),
  visible: false,
  title: 'ZONE DE BUTINAGE',
});



//Butinage (format geojson)
var pts_butinage = new ol.layer.Vector({
  source: new ol.source.Vector({
    url: '../JS/Data/Butinage.geojson',
    format: new ol.format.GeoJSON()
  }),
  visible: false,
  title: 'POINTS DE BUTINAGE',
});
/*Butinage.setStyle(
  new ol.style.Style({
    image: new ol.style.Icon({
      // For Internet Explorer 11
      imgSize: [20, 20],
      src: '../JS/Data/Abeille.png',
    }),
  })
);*/
pts_butinage.setStyle(new ol.style.Style({
  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */({
    crossOrigin: 'anonymous',
    scale: 0.2,
    src: '../JS/Data/Abeille.png'
  }))
}));


//Corine land cover Vecteur
const vectorSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function (extent) {
    return (
      'http://wxs.ign.fr/clc/geoportail/wfs?service=WFS&request=GetFeature&TYPENAME=LANDCOVER.CLC18_FR:clc18_fr&outputFormat=application/json&srsname=EPSG:3857&bbox=-81120.0447288187715458,5800584.4548332002013922,-72267.2887603089184267,5809437.5441856924444437,EPSG:3857'
    );
  },
  strategy: ol.loadingstrategy.bboxStrategy,

});

const vector = new ol.layer.Vector({
  source: vectorSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 255, 1.0)',
      width: 2,
    }),
  }),
  visible: false,
  title: 'CORINE LAND COVER',
});


//--------------------------------------------------------------------------------
//RPG
const RPGsource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function (extent) {
    return (
      'https://wxs.ign.fr/agriculture/geoportail/wfs?SERVICE=WFS&REQUEST=GetFeature&TYPENAME=RPG.2020:parcelles_graphiques&outputFormat=application/json&srsname=EPSG:3857&bbox=-81120.0447288187715458,5800584.4548332002013922,-72267.2887603089184267,5809437.5441856924444437,EPSG:3857'
    );
  },
  strategy: ol.loadingstrategy.bboxStrategy,

});

const RPGvecteur = new ol.layer.Vector({
  source: RPGsource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 255, 1.0)',
      width: 2,
    }),
  }),
  visible: false,
  title: 'RPG',
});


//------------------------------------------------------
//BD FORET
const FORETSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function (extent) {
    return (
      'https://wxs.ign.fr/environnement/geoportail/wfs?SERVICE=WFS&request=GetFeature&TYPENAME=LANDCOVER.FORESTINVENTORY.V2:bdforetv2&outputFormat=application/json&srsname=EPSG:3857&bbox=-81120.0447288187715458,5800584.4548332002013922,-72267.2887603089184267,5809437.5441856924444437,EPSG:3857'
    );
  },
  strategy: ol.loadingstrategy.bboxStrategy,

});

const FORETvecteur = new ol.layer.Vector({
  source: FORETSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 255, 1.0)',
      width: 2,
    }),
  }),
  visible: false,
  title: 'FORET',
});

//Hydrographie
const HYDROSource = new ol.source.Vector({
  format: new ol.format.GeoJSON(),
  url: function (extent) {
    return (
      'https://wxs.ign.fr/agriculture/geoportail/wfs?SERVICE=WFS&request=GetFeature&TYPENAME=HYDROGRAPHY.BCAE.2022:bcae_cours_eau&outputFormat=application/json&srsname=EPSG:3857&bbox=-81120.0447288187715458,5800584.4548332002013922,-72267.2887603089184267,5809437.5441856924444437,EPSG:3857'
    );
  },
  strategy: ol.loadingstrategy.bboxStrategy,

});

const HYDROvecteur = new ol.layer.Vector({
  source: HYDROSource,
  style: new ol.style.Style({
    stroke: new ol.style.Stroke({
      color: 'rgba(0, 0, 255, 1.0)',
      width: 2,
    }),
  }),
  visible: false,
  title: 'HYDROGRAPHIE',
});









//layers
const map = new ol.Map({

  layers: [
    BDSCAN,
    OSM,
    BDORTHO,
    pts_butinage,
    zone_butinage,
    zone_etude,
    vector,
    RPGvecteur,
    FORETvecteur,
    HYDROvecteur,
    PLEADE
    
    

  ],

  target: 'map',
  view: new ol.View({
    center: ol.proj.fromLonLat([-0.904175, 46.157005]),
    zoom: 18
  }),
});


//layer switcher
const layerSwitcher = new ol.control.LayerSwitcher({
  reverse: true,
  groupSelectStyle: 'group',
  mouseover: true
});
map.addControl(layerSwitcher);

//-----------------------------POPUP-----------------------------

// Select  interaction
var select = new ol.interaction.Select({
  hitTolerance: 5,
  multi: true,
  condition: ol.events.condition.singleClick
});
map.addInteraction(select);

// Select control
var popup = new ol.Overlay.PopupFeature({
  popupClass: 'default anim',
  select: select,
  canFix: true,

  template: {
    title:
      // 'id',   // only display the id
      function (f) {
        return f.get('id');
      },
    attributes:
    {
      'date': {
        title: 'Date : '
      },
    }
  }
});

map.addOverlay(popup);

















