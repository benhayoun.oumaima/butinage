
const Geojson_file = $.getJSON("../JS/Data/pt_butinage_8_summer.geojson", function (json) {

  const function_data = (json = []) => {
    // create map
    let map = new Map()

    for (let i = 0; i < (json.features).length; i++) {
      const s = json.features[i].properties.date;
      if (!map.has(s)) {
        map.set(s, {
          date: new Date(s).getTime(),
          value: 1,
        });
      } else {
        map.get(s).value++;
      }
    }
    const res = Array.from(map.values())
    return res;
  };
  //console.log(function_data(json))

  // json resultat contient 1010 elements sous la forme  0: {date: '"2021/11/01"', count: 1010}




  // Create root element
  // https://www.amcharts.com/docs/v5/getting-started/#Root_element
  var root = am5.Root.new("chartdiv");

  // Set themes
  // https://www.amcharts.com/docs/v5/concepts/themes/
  root.setThemes([
    am5themes_Animated.new(root)
  ]);


  data = function_data(json)
  console.log(data)

  // Create chart
  // https://www.amcharts.com/docs/v5/charts/xy-chart/
  var chart = root.container.children.push(
    am5xy.XYChart.new(root, {
      panX: true,
      panY: true,
      wheelX: "panX",
      wheelY: "zoomX"
    })
  );

  var easing = am5.ease.linear;

  // Create axes
  // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
  var xAxis = chart.xAxes.push(
    am5xy.GaplessDateAxis.new(root, {
      maxDeviation: 0.1,
      groupData: false,
      baseInterval: {
        timeUnit: "day",
        count: 1
      },
      renderer: am5xy.AxisRendererX.new(root, {
        minGridDistance: 50
      }),
      tooltip: am5.Tooltip.new(root, {})
    })
  );

  var yAxis = chart.yAxes.push(
    am5xy.ValueAxis.new(root, {
      maxDeviation: 0.1,
      renderer: am5xy.AxisRendererY.new(root, {})
    })
  );

  // Add series
  // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
  var series = chart.series.push(
    am5xy.LineSeries.new(root, {
      minBulletDistance: 10,
      xAxis: xAxis,
      yAxis: yAxis,
      valueYField: "value",
      valueXField: "date",
      tooltip: am5.Tooltip.new(root, {
        pointerOrientation: "horizontal",
        labelText: "{valueY}"
      })
    })
  );

  series.data.setAll(data);

  series.bullets.push(function () {
    return am5.Bullet.new(root, {
      sprite: am5.Circle.new(root, {
        radius: 5,
        fill: series.get("fill"),
        stroke: root.interfaceColors.get("background"),
        strokeWidth: 2
      })
    });
  });

  // Add cursor
  // https://www.amcharts.com/docs/v5/charts/xy-chart/cursor/
  var cursor = chart.set("cursor", am5xy.XYCursor.new(root, {
    xAxis: xAxis
  }));
  cursor.lineY.set("visible", false);

  // add scrollbar
  chart.set("scrollbarX", am5.Scrollbar.new(root, {
    orientation: "horizontal"
  }));

  // Make stuff animate on load
  // https://www.amcharts.com/docs/v5/concepts/animations/
  series.appear(1000, 100);
  chart.appear(1000, 100);


});

//------------- display chart with button ---------------------------




function display_chart() {

  console.log("afficher")
  div_chart = document.getElementById('chartdiv');
  console.log(div_chart)
  if (div_chart.style.visibility=="hidden")
  {
    // Contenu caché, le montrer
    div_chart.style.visibility = "visible";
    div_chart.style.height = "95vh";		
  	
  }
  else
  {
    // Contenu visible, le cacher
    div_chart.style.visibility = "hidden";
    div_chart.style.height = "0";
	
  }
  
}

