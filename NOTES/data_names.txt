pour CLC vecteur ;  le nom du fichier LANDCOVER.CLC18_FR:clc18_fr


url RPG
https://wxs.ign.fr/agriculture/geoportail/wfs?SERVICE=WFS&REQUEST=GetFeature&TYPENAME=RPG.2020:parcelles_graphiques&outputFormat=application/json&srsname=EPSG:4326

filter: ol.format.filter.like('id', 'clc06r_dom.1')


http://wxs.ign.fr/clc/geoportail/wfs?service=WFS&request=GetFeature&TYPENAME=LANDCOVER.CLC00_FR:clc00_fr&outputFormat=application/json&srsname=EPSG:3857&bbox=267822.1974218696,6231881.828642504,268710.8070304269,6232076.37918625,EPSG:3857

-72287.919089834147599, 5800566.797621245495975,-81114.89501367561752, 5809433.873248805291951
-81073.998208539938787,5800617.00378763768822,-72270.502190730607253,5809436.732735655270517
-81120.0447288187715458,5800584.4548332002013922,-72267.2887603089184267,5809437.5441856924444437

http://wxs.ign.fr/clc/geoportail/wfs?service=WFS&request=GetFeature&TYPENAME=LANDCOVER.CLC18_FR:clc18_fr&outputFormat=application/json&srsname=EPSG:3857&bbox=-81120.0447288187715458,5800584.4548332002013922,-72267.2887603089184267,5809437.5441856924444437,EPSG:3857


//data pleiades
 site esa
https://tpm-ds.eo.esa.int/socat/Pleiades/search
//IGN
https://www.geoportail.gouv.fr/donnees/pleiades-2021